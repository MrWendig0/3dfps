static const char *strings_weapons[] = {
	"Pistol",
	"Shotgun",
	"Double-barrel Shotgun",
	"Machine Gun",
	"Missile Launcher",
	"Plasma Gun",
	"The Solution"
};

static const char *strings_levels[] = {
	"Anarch Map01"
};

static const char *strings_notifs[] = { // includes obituaries and CTF messages
	"You were sliced by a Swordbot",
	"You were perforated by a Satellibot",
	"You were killed by a Spidertron",
	"You were shocked by an Electricircuit",
	// deathmatch
	"You were stabbed by a fellow gunman",
	"You were perforated by a fellow gunman",
	"You were blasted by a fellow gunman",
	"You were shredded by a fellow gunman",
	"You were swiss-cheesed by a fellow gunman",
	"You were gibbed by a fellow gunman",
	"You were shocked by a fellow gunman",
	"You were solved by a fellow gunman"
	// CTF
	"RED flag taken",
	"RED flag dropped",
	"RED flag returned",
	"BLUE flag taken",
	"BLUE flag dropped",
	"BLUE flag returned",
	// general
	"You killed a teammate",
	"A teammate killed you, go yell at them",
	"5 minutes remain",
	"1 minute remains",
	"SUDDEN DEATH",
	"You win",
	"Good game",
	"RED wins",
	"BLUE wins",
	"NOBODY wins, it's a tie",
};