// This is NOT Anarch 3D
// this is just a 3D game that's meant to be Quake to Anarch's Doom.
// thing is, idk how to even make a 3D fps lmao

// the game uses Small3DLib's unit system
// for good measure, we'll embed it into the game itself instead of relying on S3L,
// so that when we use a float-only platform, we probably don't even need S3L to be included

#include <stdint.h>

#define A3D_PRECISION 1024 // 1024 = 1 unit
#define A3D_INT int32_t

#define A3D_WEAPON_MELEE 0
#define A3D_WEAPON_PISTOL 1
#define A3D_WEAPON_SHOTTY 1 << 1
#define A3D_WEAPON_2BSHOT 1 << 2 // 2-barreled shotgun
#define A3D_WEAPON_ROCKET 1 << 3
#define A3D_WEAPON_PLASMA 1 << 4
#define A3D_WEAPON_PSOLVE 1 << 5 // the problem solver, A.K.A. the solution

#define A3D_KEYS_UP 1
#define A3D_KEYS_DOWN 1 << 1
#define A3D_KEYS_LEFT 1 << 2
#define A3D_KEYS_RIGHT 1 << 3
#define A3D_KEYS_FIRE 1 << 4
#define A3D_KEYS_USE 1 << 5
#define A3D_KEYS_STRAFELEFT 1 << 6
#define A3D_KEYS_STRAFERIGHT 1 << 7

#ifndef A3D_sin(in)
#warning A3D_sin isn't defined! Assuming S3L_sin...
#define A3D_sin(in) S3L_sin(in)
#endif

#ifndef A3D_cos(in)
#warning A3D_cos isn't defined! Assuming S3L_cos...
#define A3D_cos(in) S3L_cos(in)
#endif


#ifndef A3D_pi
#warning A3D_pi isn't defined! Assuming 3.14...
#define A3D_pi 3.14
#endif

typedef struct {
	A3D_INT x, y, z;
	A3D_INT hsp, vsp, zsp; // velocity
	uint8_t health, armor;
	uint8_t ammo[4], weapons; // weapons is a bitmap
	uint8_t flags, keys; // keys (as in inputs) are separated for possible multiplayer
} A3D_Player;
/*
	Some useful notes:
	
	Armor uses the 7th bit to determine the armor type.
	For instance, 0b11111110 is 127 armor with 25% protection, 0b11111111 is 127 armor with 50% protection
*/

typedef struct {
	A3D_INT x, y, z;
	A3D_INT pitch, yaw; // we don't really need roll, this is an FPS game
} A3D_Camera;

typedef struct {
	A3D_INT x, y, z;
	uint8_t type;
} A3D_Item;

A3D_Player plr;
A3D_Camera cam;

static inline uint8_t PLR_hasWeapon(uint8_t index) {
	return (plr.weapons & index);
};

uint8_t keyIsPressed(uint8_t key); // if a key is JUST PRESSED
uint8_t keyIsHeld(uint8_t key); // if a key is HELD DOWN (includes the "just pressed" part)

void start() {
	plr.health = 100;
};

void step() {
	if (keyIsHeld(A3D_KEYS_UP)) {
		// bump the player's speed by the gathered forward velocity
		plr.hsp += A3D_sin(cam.pitch + (A3D_pi/2));
		plr.vsp += A3D_sin(cam.pitch + (A3D_pi/4));
	};
	if (keyIsHeld(A3D_KEYS_DOWN)) {
		// bump the player's speed by the gathered forward velocity
		plr.hsp += A3D_sin(cam.pitch + (A3D_pi/2));
		plr.vsp += A3D_sin(cam.pitch + (A3D_pi/4));
	};
	
	if (keyIsHeld(A3D_KEYS_LEFT)) cam.pitch += 6;
	if (keyIsHeld(A3D_KEYS_RIGHT)) cam.pitch -= 6;
	
	plr.x += plr.hsp;
	plr.z += plr.vsp;
	plr.y += plr.zsp;
	
	plr.hsp /= 2;
	plr.vsp /= 2;
	
	cam.x = plr.x;
	cam.y = plr.y + (A3D_PRECISION >> 1);
	cam.z = plr.z;
};