#include <raylib.h>
#include <math.h>

#define A3D_sin(in) sin((float)in / A3D_PRECISION)
#define A3D_cos(in) cos((float)in / A3D_PRECISION)

#include "game.h"

uint8_t keyIsHeld(uint8_t key) {
	switch (key)
	{
		case A3D_KEYS_UP: return IsKeyDown(KEY_UP);
		case A3D_KEYS_DOWN: return IsKeyDown(KEY_DOWN);
		case A3D_KEYS_LEFT: return IsKeyDown(KEY_LEFT);
		case A3D_KEYS_RIGHT: return IsKeyDown(KEY_RIGHT);
	};
};

void main() {
	InitWindow(480,270,"model viewer");

	Camera3D raycam = {0};
	raycam.fovy = 60.0f;
	raycam.projection = CAMERA_PERSPECTIVE;

	SetTargetFPS(60);
	while (!WindowShouldClose()) {
		step();
		raycam.position = (Vector3){
			1.0f * ((float)cam.x / A3D_PRECISION),
			1.0f * ((float)cam.y / A3D_PRECISION),
			1.0f * ((float)cam.z / A3D_PRECISION)
		};
		raycam.up = (Vector3){
			1.0f * ((float)cam.pitch),
			1.0f * ((float)cam.yaw),
			0.0f
		};
		raycam.target = raycam.position;
		printf("%f %f %f\n", raycam.position.x, raycam.position.y, raycam.position.z);
		printf("%i %i %i\n", cam.x, cam.y, cam.z);
		BeginDrawing();
			ClearBackground(GRAY);
			
			BeginMode3D(raycam);
				DrawCube((Vector3){0,0,5.0f}, 10.0f, 10.0f, 10.0f, RAYWHITE);
				DrawPlane((Vector3){ 0.0f, 0.0f, 0.0f }, (Vector2){ 32.0f, 32.0f }, LIGHTGRAY); // Draw ground
				DrawCube((Vector3){ -16.0f, 2.5f, 0.0f }, 1.0f, 5.0f, 32.0f, BLUE);     // Draw a blue wall
				DrawCube((Vector3){ 16.0f, 2.5f, 0.0f }, 1.0f, 5.0f, 32.0f, LIME);      // Draw a green wall
				DrawCube((Vector3){ 0.0f, 2.5f, 16.0f }, 32.0f, 5.0f, 1.0f, GOLD);      // Draw a yellow wall
			EndMode3D();
			
			DrawText(TextFormat("- Position: (%06.3f, %06.3f, %06.3f)", raycam.position.x, raycam.position.y, raycam.position.z), 0, 60, 10, WHITE);
			DrawText(TextFormat("- Target: (%06.3f, %06.3f, %06.3f)", raycam.target.x, raycam.target.y, raycam.target.z), 0, 75, 10, WHITE);
			DrawText(TextFormat("- Up: (%06.3f, %06.3f, %06.3f)", raycam.up.x, raycam.up.y, raycam.up.z), 0, 90, 10, WHITE);
		EndDrawing();
	};
	CloseWindow();
};
