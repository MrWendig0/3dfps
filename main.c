#define S3L_RESOLUTION_X 480
#define S3L_RESOLUTION_Y 270
#define S3L_NEAR_CROSS_STRATEGY 3

#define S3L_PERSPECTIVE_CORRECTION 2

#define S3L_NEAR (S3L_F / 5)

#define S3L_USE_WIDER_TYPES 1
#define S3L_FLAT 0
#define S3L_SORT 0
#define S3L_Z_BUFFER 1
#define S3L_MAX_TRIANGES_DRAWN 2048

#define S3L_PIXEL_FUNCTION drawPixel

#define TEXTURES 1

#define TEXTURE_W 64
#define TEXTURE_H 64

#include "small3dlib.h"

#include "carModel.h"
#include "cityTexture.h"

#define A3D_sin(in) S3L_sin(in)
#define A3D_cos(in) S3L_cos(in)
#define A3D_pi S3L_FRACTIONS_PER_UNIT

#include "game.h"

S3L_Scene scene;
S3L_Model3D models[1];

#include "sdl_helper.h"

uint32_t previousTriangle = -1;
S3L_Vec4 uv0, uv1, uv2;
const uint8_t *texture;
const S3L_Index *uvIndices;
const S3L_Unit *uvs;

void drawPixel(S3L_PixelInfo *p)
{
  uint8_t r, g, b;

#if TEXTURES
  if (p->triangleID != previousTriangle)
  {
	switch (p->modelIndex)
	{
		case 0: texture = cityTexture; uvIndices = carUVIndices; uvs = carUVs; break;
		break;
	}

    S3L_getIndexedTriangleValues(p->triangleIndex,uvIndices,uvs,2,&uv0,&uv1,&uv2);
    previousTriangle = p->triangleID;
  }

  S3L_Unit uv[2];

  uv[0] = S3L_interpolateBarycentric(uv0.x,uv1.x,uv2.x,p->barycentric) / 16;
  uv[1] = S3L_interpolateBarycentric(uv0.y,uv1.y,uv2.y,p->barycentric) / 16;

  sampleTexture(texture,uv[0],uv[1],&r,&g,&b);
#else
  switch (p->modelIndex)
  {
    case 0: r = 255; g = 0; b = 0; break;
    case 1: r = 0; g = 255; b = 0; break;
    case 2:
    default: r = 0; g = 0; b = 255; break;
  }
#endif

#if FOG
  S3L_Unit fog = (p->depth * 
#if TEXTURES
    8
#else
    16
#endif
  ) / S3L_F;

  r = S3L_clamp(((S3L_Unit) r) - fog,0,255);
  g = S3L_clamp(((S3L_Unit) g) - fog,0,255);
  b = S3L_clamp(((S3L_Unit) b) - fog,0,255);
#endif

  setPixel(p->x,p->y,r,g,b); 
}

uint8_t *keys;

uint8_t keyIsHeld(uint8_t key) {
	switch (key)
	{
		case A3D_KEYS_UP: return keys[SDL_SCANCODE_UP];
		case A3D_KEYS_DOWN: return keys[SDL_SCANCODE_DOWN];
		case A3D_KEYS_LEFT: return keys[SDL_SCANCODE_LEFT];
		case A3D_KEYS_RIGHT: return keys[SDL_SCANCODE_RIGHT];
	};
};

void main() {
	sdlInit();
	
	S3L_Unit cubeVertices[] = { S3L_CUBE_VERTICES(S3L_F) };
	S3L_Index cubeTriangles[] = { S3L_CUBE_TRIANGLES };

	carModelInit();
	models[0] = carModel;
	#define CARSCALE 6 * S3L_F
	models[0].transform.scale = (S3L_Vec4){CARSCALE, CARSCALE, CARSCALE, CARSCALE};
	#undef CARSCALE
	
	S3L_sceneInit(&models, 1, &scene);
	
	uint8_t running = 1;
	keys = SDL_GetKeyboardState(NULL);
	SDL_Event event;
	while (running) {
		S3L_newFrame();
		while (SDL_PollEvent(&event)) {
		  if (event.type == SDL_QUIT) {
			running = 0;
		  };
		};
		step();
		scene.camera.transform.translation.x = cam.x;
		scene.camera.transform.translation.y = cam.y;
		scene.camera.transform.translation.z = cam.z;
		scene.camera.transform.rotation.x = 0;
		scene.camera.transform.rotation.y = cam.pitch;
		scene.camera.transform.rotation.z = cam.yaw;
		clearScreen();
		S3L_drawScene(scene);
		S3L_logTransform3D(scene.camera.transform);
		sdlUpdate();
		SDL_Delay(16.667f);
	};
	
	sdlEnd();
};
